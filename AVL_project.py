#AVL project

# 1 - Creating a tree node
class AVLNode(object):
    def __init__(self, key):
        self.key = key
        self.left = None
        self.right = None
        self.height = 1

class AVLTree(object):

    # Calculate the height of the node/tree
    def TreeHeight(self, root):
        if not root:
            return 0
        return root.height
    
    # Calculate balance score of the node/tree
    def balanceScore(self, root):
        if not root:
            return 0
        return self.TreeHeight(root.left) - self.TreeHeight(root.right)
    
    #  Adding a node to tree
    def add_node(self, root, key):

        # Find the correct location and insert the node
        if not root:
            return TreeNode(key)
        elif key < root.key:
            root.left = self.add_node(root.left, key)
        else:
            root.right = self.add_node(root.right, key)

        root.height = 1 + max(self.TreeHeight(root.left),
                              self.TreeHeight(root.right))

        # Update the balance_score and balance the tree
        updateScore = self.balanceScore(root)
        if updateScore > 1:
            if key < root.left.key:
                return self.rotate_Right(root)
            else:
                root.left = self.rotate_Left(root.left)
                return self.rotate_Right(root)

        if updateScore < -1:
            if key > root.right.key:
                return self.rotate_Left(root)
            else:
                root.right = self.rotate_Right(root.right)
                return self.rotate_Left(root)

        return root
    
    #  Removing the node
    def remove_node(self, root, key):

        # Find the node to be deleted and remove it
        if not root:
            return root
        elif key < root.key:
            root.left = self.remove_node(root.left, key)
        elif key > root.key:
            root.right = self.remove_node(root.right, key)
        else:
            if root.left is None:
                temp = root.right
                root = None
                return temp
            elif root.right is None:
                temp = root.left
                root = None
                return temp
            temp = self.calc_node_value(root.right)
            root.key = temp.key
            root.right = self.remove_node(root.right,
                                          temp.key)
        if root is None:
            return root

        # Updating the balance_score of nodes
        root.height = 1 + max(self.TreeHeight(root.left),
                              self.TreeHeight(root.right))

        updateScore = self.balanceScore(root)

        # Balancing the tree after removal
        if updateScore > 1:
            if self.balanceScore(root.left) >= 0:
                return self.rotate_Right(root)
            else:
                root.left = self.rotate_Left(root.left)
                return self.rotate_Right(root)
        if updateScore < -1:
            if self.balanceScore(root.right) <= 0:
                return self.rotate_Left(root)
            else:
                root.right = self.rotate_Right(root.right)
                return self.rotate_Left(root)
        return root
    
    # Rotating tree
    # Rotate Left
    def rotate_Left(self, z):
        y = z.right
        T2 = y.left
        y.left = z
        z.right = T2
        z.height = 1 + max(self.TreeHeight(z.left),
                           self.TreeHeight(z.right))
        y.height = 1 + max(self.TreeHeight(y.left),
                           self.TreeHeight(y.right))
        return y

    # Rotate Right
    def rotate_Right(self, z):
        y = z.left
        T3 = y.right
        y.right = z
        z.left = T3
        z.height = 1 + max(self.TreeHeight(z.left),
                           self.TreeHeight(z.right))
        y.height = 1 + max(self.TreeHeight(y.left),
                           self.TreeHeight(y.right))
        return y


    def calc_node_value(self, root):
        if root is None or root.left is None:
            return root
        return self.calc_node_value(root.left)

    def preOrder(self, root):
        if not root:
            return
        print("{0} ".format(root.key), end="")
        self.preOrder(root.left)
        self.preOrder(root.right)